#include "qmldownloadfile.h"
#include <QDir>

QmlDownloadFile::QmlDownloadFile(QObject *parent) : QObject(parent)
{
    qdownloadFile = new QDownloadFile();
    qdownloadFile->moveToThread(&downloadthread);


    connect(qdownloadFile,&QDownloadFile::successDownload,this,&QmlDownloadFile::successDownload);
    connect(qdownloadFile,&QDownloadFile::failedDownload,this,&QmlDownloadFile::failedDownload);

    connect(qdownloadFile,&QDownloadFile::updateDownloadProgress,this,&QmlDownloadFile::updateDownloadProgress);    
    connect(&downloadthread,&QThread::destroyed,qdownloadFile,&QDownloadFile::deleteLater);
    downloadthread.start();
}

void QmlDownloadFile::startDownload(QString filename){

    savePath = QDir::tempPath() + "/" + filename;
    QMetaObject::invokeMethod(qdownloadFile,"startDownload",Q_ARG(QString,urlAddress + filename),Q_ARG(QString,savePath));
}

QmlDownloadFile::~QmlDownloadFile(){
    //qdownloadFile->cancelDownload();
    QMetaObject::invokeMethod(qdownloadFile,"cancelDownload");
    //qDebug() << "destr";
    downloadthread.quit();
    downloadthread.wait();
    //qDebug() << "end destr";
    QFile::remove(savePath);
}
