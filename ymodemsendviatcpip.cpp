#include "ymodemsendviatcpip.h"
#include <QDataStream>
#include <QDateTime>
#include <QDir>

#include <QSettings>

const QString key = "dedq8ytrbva4fuc";
const QString sharedSecret = "hv63a7xt4yxa408";

YmodemSendViaTCPIP::YmodemSendViaTCPIP(QObject *parent) : QObject(parent),tcpsocket(nullptr),ymodemProtocol(nullptr),localFile(nullptr),logFile(nullptr)
{


}

void YmodemSendViaTCPIP::init(){

    logFile = new QFile(this);
    logFile->setFileName("updater.log");
    QSettings *cnf = new QSettings("settings.ini", QSettings::IniFormat);

    mfirmwareB1 = cnf->value("firmwareB1","EG_B1.bin").toString();
    mfirmwareB2 = cnf->value("firmwareB2","EG_B2.bin").toString();


    cnf->setValue("firmwareB1",mfirmwareB1);
    cnf->setValue("firmwareB2",mfirmwareB2);

    emit sendFirmwareNames(mfirmwareB1,mfirmwareB2);

    delete cnf;

    tcpsocket = new QTcpSocket(this);
    ymodemProtocol = new YmodemProtocol(this);

    connect(ymodemProtocol,&YmodemProtocol::sendResponseResult,this,&YmodemSendViaTCPIP::sendStatus);

    localFile = new QFile(this);
}

void YmodemSendViaTCPIP::setUseLocalFile(bool useLocalFileB1, bool useLocalFileB2){
    if(museLocalFileB1 != useLocalFileB1){
        museLocalFileB1 = useLocalFileB1;
    }
    if(museLocalFileB2 != useLocalFileB2){
        museLocalFileB2 = useLocalFileB2;
    }
}

void YmodemSendViaTCPIP::setIp(QString ip){

    mip.setAddress(ip);
}

void YmodemSendViaTCPIP::setPort(quint16 port){
    if(mport != port)
        mport = port;
}

void YmodemSendViaTCPIP::setFirmwareFileName(QString fileName){
    if(this->mFirmwareFileName != fileName){
        this->mFirmwareFileName = fileName ;
        emit firmwareFileNameChange(mFirmwareFileName);
    }
}

QString YmodemSendViaTCPIP::ip(){
    return mip.toString();
}

QString YmodemSendViaTCPIP::firmwareFileName(){
    return mFirmwareFileName;
}

QString YmodemSendViaTCPIP::firmware1(){
    return mfirmwareB1;
}

QString YmodemSendViaTCPIP::firmware2(){
    return mfirmwareB2;
}

QString YmodemSendViaTCPIP::firmwareDevice(){
    return mFirmwareDevice;
}

quint16 YmodemSendViaTCPIP::port(){
    return mport;
}

QByteArray YmodemSendViaTCPIP::readProcess(){
    tcpsocket->waitForReadyRead(waitingTime);
    QByteArray message;
    if(tcpsocket->bytesAvailable()){

        message = tcpsocket->readAll();
        logFile << ("read bytes " + message.toHex())  << "\r\n";
        return message;
    }

    message.push_back(0xff);
    logFile->write("read bytes " + message.toHex() + "\r\n");
    return message;
}

void YmodemSendViaTCPIP::writeProcess(const QByteArray &ba){
    if(connected){
        tcpsocket->write(ba);
        tcpsocket->waitForBytesWritten(1000);
    }
}

void YmodemSendViaTCPIP::writeProcess(const int c){
    if(connected){
        QByteArray ba;
        ba.append(QChar(c));
        tcpsocket->write(ba);
        tcpsocket->waitForBytesWritten(1000);
    }
}

YMODEMResponce YmodemSendViaTCPIP::transmitPacket(const QString &fileName, const QString &fileSize, const QString &fileDateStamp, QIODevice *dev, quint8 sequenceNum, bool firstPacket){

    if (dev->atEnd()){
        for (int retry = 0; retry < maxAttemp; ++retry) {
            logFile->write(QString("write EOT " + QString::number(retry) + "\r\n").toLocal8Bit());
            writeProcess(EOT);
            char res = readProcess().at(0);
            if (res == ACK) {
                logFile->write("return ACK\r\n");
                return YMODEMResponce::MODEM_SUCCESS_WRITE;
            }
            else{
                logFile->write(QString("return " + QString::number(res) + "\r\n").toLocal8Bit());
            }
        }
        return YMODEMResponce::MODEM_EOT_ERROR;
    }

    QByteArray payload;

    if (firstPacket){
        logFile->write("first packet\r\n");
         payload = ymodemProtocol->createFirstPayload(fileName,fileSize,fileDateStamp);
    }
    else{
        logFile->write(QString("packet pos" + QString::number( dev->pos()) + "\r\n").toLocal8Bit());
        payload = ymodemProtocol->createPayload(dev);
    }

        QByteArray packet = ymodemProtocol->createPacket(payload,sequenceNum);

        int pos = dev->pos();
        //logFile->write("XMODEM:xBeginTransmit - Packet # : " + QString::number( pos/YmodemProtocol::payloadSize) + " / " + QString::number((int)floor((float)fileInfo.size()/YmodemProtocol::payloadSize + 0.5f)));

        emit progress(pos);

        for (int retry = 0; retry < maxAttemp && !m_cancel; ++retry) {
            writeProcess(packet);
            char res = readProcess().at(0);
            logFile->write(QString("retry" + QString::number(retry) + "\r\n").toLocal8Bit());
            if (res >= 0 ) {
                switch (res) {
                case ACK:
                    logFile->write("ACK\r\n");
                    return transmitPacket(mFirmwareFileName,fileSize,fileDateStamp,dev,sequenceNum+1,false);
                case CAN:
                    logFile->write("CAN\r\n");
                    res = readProcess().at(0);
                    if (res == CAN) {
                        logFile->write("ACK\r\n");
                        writeProcess(ACK);
                        logFile->write("XMODEM::xBeginTransmit - canceled by remote\r\n");
                        emit sendStatus("XMODEM::xBeginTransmit - canceled by remote");
                        return YMODEMResponce::MODEM_REMOTE_CANCEL;
                    }
                    break;
                case NAK:
                    logFile->write("NAK\r\n");
                    break;
                case 'c':
                case 'C':
                    logFile->write("C\r\n");
                    retry--;
                    continue;
                case '>':
                    logFile->write("restart\r\n");
                    return YMODEMResponce::MODEM_RESTART;
                case '\0':
                    logFile->write("\\0\r\n");
                    break;
                default:
                    logFile->write(QString("XYMODEM::yTransmitPacket - Unknown " + QString::number(res) + "\r\n").toLocal8Bit());
                    break;
                }
            }
        }
        logFile->write("CAN\r\n");
        writeProcess(CAN);
        writeProcess(CAN);
        writeProcess(CAN);

    return YMODEMResponce::MODEM_PACKET_FAILED;
}

void YmodemSendViaTCPIP::stopSend(){
    inWork = false;
    qDebug() << "logFile.close()" << logFile->isOpen();
    logFile->close();
    localFile->close();
    tcpsocket->disconnectFromHost();
    QFile::remove(QDir::tempPath() + "/" + mFirmwareFileName);
}

void YmodemSendViaTCPIP::startSend(){

    if(inWork)
        return;

    emit sendStatus("");
    emit progress(0);

    inWork = true;

    logFile->open(QIODevice::WriteOnly);

    logFile->write(QString("ip " + mip.toString() + " port " + QString::number(mport) + "\r\n").toLocal8Bit());

    emit sendStatus("Waiting for connection");

    qDebug() << mip << mport;

    tcpsocket->connectToHost(mip,mport);
    connected = false;

    if(!tcpsocket->waitForConnected(5000)){
        logFile->write("not connected to host\r\n");
        emit sendStatus("not connected to host");

        stopSend();
        return;
    }

    connected = true;
    emit sendStatus("Waiting answer");
    tcpsocket->waitForReadyRead(waitingTime);
    QByteArray data = tcpsocket->readAll();

    qDebug() << "data " << data;
    logFile->write("data " + data + "\r\n");

    if(data.contains("B1")){
        setFirmwareFileName( mfirmwareB2 );
        mFirmwareDevice = data;
        emit firmwareDeviceChange(mFirmwareDevice);
    }
    else if(data.contains("B2")){
        setFirmwareFileName( mfirmwareB1 );
        mFirmwareDevice = data;
        emit firmwareDeviceChange(mFirmwareDevice);
    }
    else{

        mFirmwareDevice = "No Firmware";
        emit firmwareDeviceChange(mFirmwareDevice);
        emit requestChooseFirmwareName();

        QEventLoop loop;
        connect(this,&YmodemSendViaTCPIP::resumeThread,&loop,&QEventLoop::quit);
        connect(this,&YmodemSendViaTCPIP::resumeThread,[=](){qDebug() << "loop quit";});
        loop.exec();
        disconnect(this,&YmodemSendViaTCPIP::resumeThread,&loop,&QEventLoop::quit);
    }

    tcpsocket->write("1\r\n");
    tcpsocket->waitForBytesWritten();

    if(!tcpsocket->waitForReadyRead(waitingTime)){
        logFile->write("not READY");
        emit sendStatus("not READY");
        stopSend();
        return;
    }

    data = tcpsocket->readAll();
    logFile->write("data " + data + "\r\n");

    QString fileSize,fileDateStamp;


    if(mFirmwareFileName == mfirmwareB1 && museLocalFileB1)
        localFile->setFileName( mFirmwareFileName);
    else
        localFile->setFileName(QDir::tempPath() + "/" + mFirmwareFileName);

    if(mFirmwareFileName == mfirmwareB2 && museLocalFileB2)
        localFile->setFileName( mFirmwareFileName);
    else
        localFile->setFileName(QDir::tempPath() + "/" + mFirmwareFileName);

    qDebug() << "local filename" << localFile->fileName();

    if(!localFile->open(QIODevice::ReadOnly)){
                logFile->write(QString("can't open file " + mFirmwareFileName + "\r\n").toLocal8Bit());
                emit sendStatus("can't open file");
                stopSend();
                return;
            }
            else{
                QFileInfo fileInfo(localFile->fileName());
                fileSize = QString::number(fileInfo.size());
                fileDateStamp = QString::number(fileInfo.created().toTime_t());

                qDebug() << "send file size" << fileSize << fileDateStamp;
                emit sendFileSize(fileInfo.size());
                emit sendStatus(QString("Sending Updates ..."));
                logFile->write(QString("send file size " + fileSize + "\r\n").toLocal8Bit());
            }

    for(int retry = 0; retry < maxAttemp && !m_cancel; ++retry){

        char message = readProcess().at(0);
        switch(message){
            case '\0':
                logFile->write("read \0\r\n");
            break;

            case 'c':
            case 'C':
                logFile->write("read C\r\n");
                YMODEMResponce response;

                    response = transmitPacket(mFirmwareFileName,fileSize,fileDateStamp,localFile);

                ymodemProtocol->checkResponse(response);

                if (response == YMODEMResponce::MODEM_RESTART){
                    qDebug() << "restart";

                    localFile->seek(0);


                    retry = 0;
                    continue;
                }
                else if(response == YMODEMResponce::MODEM_SUCCESS_WRITE) {
                    ymodemProtocol->checkResponse(transmitClose());
                    retry = maxAttemp;
                }
                else {
                    logFile->write("unseccuss write\r\n");

                    retry = maxAttemp;
                }
            break;
        case NAK:{
            logFile->write("NAK\r\n");
            YMODEMResponce response;

            response = transmitPacket(mFirmwareFileName,fileSize,fileDateStamp,localFile);

            if (response == YMODEMResponce::MODEM_RESTART){
                localFile->seek(0);

                retry = 0;
                continue;
            }
        break;
        }
        case CAN:
            logFile->write("XMODEM::yTransmit - canceled by remote\r\n");
            emit sendStatus("Transmit - canceled by remote");
        break;
        default:
            logFile->write(QString("unknown message " + QString::number((int)message) + "\r\n").toLocal8Bit());
            emit sendStatus("unknown message " + QString::number((int)message));
        }
    }
    logFile->write("file close\r\n");

   stopSend();
}


YMODEMResponce YmodemSendViaTCPIP::transmitClose(){

    qDebug() << "transimt close";

    for (int retry = 0; retry < maxAttemp; ++retry) {
        QByteArray message = readProcess();
        char res = message.at(0);
        logFile->write(QString( "start send zero" + QString::number(retry) + "\r\n").toLocal8Bit());
        qDebug() << "start send zero";
        if (res >= 0 ) {
            switch (res) {
            case ACK:{
                logFile->write( "ACK\r\n");
                if(retry != 0){
                    retry = maxAttemp;
                    logFile->write(QString("retry " + QString::number(retry) + "\r\n").toLocal8Bit());
                    logFile->write(QString("message " + message + "\r\n").toLocal8Bit());
                    if(message.contains("SUCCESS")){
                        logFile->write("SUCCESS\r\n");
                        return YMODEMResponce::MODEM_SUCCESS_UPDATE;
                    }
                    else{
                        logFile->write("no success\r\n");
                        message = readProcess();
                        logFile->write("message " + message + "\r\n");
                        if(message.contains("SUCCESS")){
                            logFile->write("success update\r\n");
                            return YMODEMResponce::MODEM_SUCCESS_UPDATE;
                        }
                        else{
                            logFile->write("unsuccessfull update\r\n");
                            return YMODEMResponce::MODEM_NOT_SUCCESS_UPDATE;
                        }
                    }
                }
                break;
            }
            case NAK:
                logFile->write("read NAK\r\n");
            case 'c':
            case 'C':{
                logFile->write("read C\r\n");
                logFile->write("write zero\r\n");
                writeProcess(ymodemProtocol->createClosePacket());
                break;
            }
            case '>':
                logFile->write("restart\r\n");
                return YMODEMResponce::MODEM_RESTART;
            case '\0':
                logFile->write("\\0\r\n");
                break;
            default:
                logFile->write(QString("XYMODEM::yTransmitPacket - Unknown " + QString::number((int)res) + "\r\n").toLocal8Bit());
                break;
            }
        }
    }
}

void YmodemSendViaTCPIP::cancelSend(){
    m_cancel = true;
    connected = true;

    //stopSend();
}

YmodemSendViaTCPIP::~YmodemSendViaTCPIP(){
    cancelSend();
    stopSend();
}
