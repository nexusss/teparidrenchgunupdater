#ifndef YMODEMSENDVIATCPIP_H
#define YMODEMSENDVIATCPIP_H

#include <QObject>
#include <QTcpSocket>
#include <QDebug>
#include <QFileInfo>
#include <QHostAddress>
#include <QFile>
#include <QTextStream>
#include <QEventLoop>
#include <QThread>
#include "ymodemprotocol.h"

class YmodemSendViaTCPIP : public QObject
{
    Q_OBJECT

    QHostAddress mip = QHostAddress("10.10.10.1");
//    QHostAddress mip = QHostAddress("192.168.1.37");
//    QHostAddress mip = QHostAddress("127.0.0.7");
    quint16 mport = 2000;
    QString mFirmwareFileName;
    QString mFirmwareDevice;
    QString mfirmwareB1;
    QString mfirmwareB2;

    QTcpSocket *tcpsocket;
    const quint32 waitingTime = 10 * 1000;//in seconds
    const quint32 maxAttemp = 10;

    bool inWork = false;

    bool connected = false;
    bool m_cancel = false;

    bool museLocalFileB1 = false;
    bool museLocalFileB2 = false;


    YmodemProtocol *ymodemProtocol;

    QFile *logFile;


    QFile *localFile;


    QString mDeviceFirmware;
    Q_PROPERTY(quint16 port READ port WRITE setPort )
    QByteArray readProcess();
    void writeProcess(const QByteArray &ba);
    void writeProcess(const int c);    

    void stopSend();

public:
    explicit YmodemSendViaTCPIP(QObject *parent = 0);
    ~YmodemSendViaTCPIP();
    QString ip();
    QString firmwareFileName();
    QString firmware1();
    QString firmware2();
    QString firmwareDevice();
    quint16 port();
    void cancelSend();

signals:
    void sendComplete();
    void sendStatus(QString status);
    void progress(double pos);
    void sendFileSize(double fileSize);    
    void requestChooseFirmwareName();
    void resumeThread();
    void firmwareFileNameChange(QString firmwareFileName);
    void firmwareDeviceChange(QString firmwareDevice);    
    void sendFirmwareNames(QString,QString);


public slots:
    void init();
    void setIp(QString ip);
    void setPort(quint16 port);
    void setFirmwareFileName(QString firmwareFileName);
    void startSend();
    void setUseLocalFile(bool useLocalFileB1,bool useLocalFileB2);

private:
    YMODEMResponce transmitClose();
    YMODEMResponce transmitPacket(const QString &fileName, const QString &fileSize, const QString &fileDateStamp, QIODevice *dev, quint8 sequenceNum = 0, bool firstPacket = true);
};

#endif // YMODEMSENDVIATCPIP_H
