QT += qml quick network xml

CONFIG += c++11

TARGET = TePariDrenchGunUpd

SOURCES += main.cpp \
    ymodemsendviatcpip.cpp \
    ymodemprotocol.cpp \
    drenchgunupdater.cpp \
    qdownloadfile.cpp \
    qmldownloadfile.cpp

RESOURCES += qml.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Default rules for deployment.
include(deployment.pri)

RC_ICONS = images/TEPARI_RGB.ico

HEADERS += \
    ymodemsendviatcpip.h \
    ymodemprotocol.h \
    drenchgunupdater.h \
    qdownloadfile.h \
    qmldownloadfile.h

win32: LIBS += -L$$PWD/openssl/lib -leay32

INCLUDEPATH += $$PWD/openssl/include
DEPENDPATH += $$PWD/openssl/include
