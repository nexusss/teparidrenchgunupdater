import QtQuick 2.0
import QtQuick.Controls 2.1

import QtQuick.Layouts 1.1

Rectangle {

    signal useLocalFile()
    signal cancel()
    property string firmwareName: ""
    color: "#00000000"

    Rectangle{
        color: "#a4a4a4"
        opacity: 0.8
        anchors.fill: parent
    }



    Rectangle {
        y: 100

        width: 250
        height: 100
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter

    ColumnLayout {
        id: columnLayout
        width: 20
        height: 10
        anchors.fill: parent

        Text {
            id: text1
            text: qsTr("Could not load:")
            Layout.fillWidth: true
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            Layout.fillHeight: true
            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
            font.pixelSize: 13
        }
        Text {
            id: text2
            text: firmwareName
            font.pixelSize: 13
            Layout.fillWidth: true
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            Layout.fillHeight: true
        }

        RowLayout {
            id: rowLayout
            y: 110
            width: 100
            height: 100
            Layout.fillHeight: true

            Layout.fillWidth: true
            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter

            Button {
                id: localButton
                text: qsTr("Use Local File")
                onClicked: useLocalFile()
            }

            Button {
                id: cancelButton
                text: qsTr("Cancel")
                onClicked: cancel()
            }
        }
    }
}
}
