import QtQuick 2.0
import QtQuick.Layouts 1.1
import QtQuick.Controls 2.1
import QtGraphicalEffects 1.0
import Tepari.downloader 1.0


Rectangle {
    signal nextPage()
    signal setLocalFile(bool localFirmwareB1,bool localFirmwareB2)

    property string firmwareB1: ""
    property string firmwareB2: ""

    property int firmwareB1OK: -1
    property int firmwareB2OK: -1

    width: 720
    property alias dropShadow: dropShadow

    function startDownloadFirmware(){
        b1Downloader.startDownload(firmwareB1)
        b2Downloader.startDownload(firmwareB2)
    }

    function checkFirmwareState(){
        if (firmwareB1OK === -1 || firmwareB2OK === -1 ||
            firmwareB1OK === 1 || firmwareB2OK === 1    ){
            return
        }

        if(firmwareB1OK == 0)
            connectToServer.firmwareName = firmwareB1 + "\r\n"

        if(firmwareB2OK == 0)
            connectToServer.firmwareName += firmwareB2

        connectToServer.visible = true
    }

    Text {
        id: text1
        x: 36
        y: 8
        text: qsTr("Update Instructions:")
        font.pixelSize: 24
    }

    Connecting{
        id: connectToServer

        z: 1
        anchors.fill: parent
        visible: false
        onCancel: Qt.quit()
        onUseLocalFile: {

            connectToServer.visible = false

            if (firmwareB1OK === 0 || firmwareB2OK === 0){
                setLocalFile(firmwareB1OK === 0, firmwareB2OK === 0)
                firmwareB1OK = 1
                firmwareB2OK = 1
            }
        }
    }

    TepariDownloader{
        id: b1Downloader
        onUpdateDownloadProgress: {
            progressBarB1.value = bytesReceived
            progressBarB1.to = bytesTotal
        }
        onSuccessDownload: {
            firmwareB1OK = 1
            checkFirmwareState()
        }
        onFailedDownload: {
            firmwareB1OK = 0
            checkFirmwareState()
        }
    }

    TepariDownloader{
        id: b2Downloader
        onUpdateDownloadProgress: {
            progressBarB2.value = bytesReceived
            progressBarB2.to = bytesTotal
        }
        onSuccessDownload: {
            firmwareB2OK = 1
            checkFirmwareState()
        }
        onFailedDownload: {
            firmwareB2OK = 0
            checkFirmwareState()
        }
    }

    ListModel {
        id: listModel
        ListElement {
            index: "1"
            description: "Press and hold the DOWN arrow while turning on the Drench Gun"

        }
        ListElement {
            index: "2"
            description: "Then press the STATS button and wait for the Drench Gun to enter Upgrade mode"

        }
        ListElement {
            index: "3"
            description: "Connect to the tepari_EG_upgrade network"
        }
        ListElement {
            index: "4"
            description: "Once connected Click next"
        }
    }

    Component {
        id: contactDelegate
        Item {
            width: listView.width
            height: descriptText.height + 20 < listView.height / listModel.count ? listView.height / listModel.count :descriptText.height + 20
            RowLayout {
                width: parent.width
                Text {
                    wrapMode: Text.WordWrap
                    font.pixelSize: 22
                    text: index + ":"
                }
                Text {
                    id: descriptText
                    wrapMode: Text.WordWrap
                    font.pixelSize: 16
                    text: description
                    Layout.fillHeight: true
                    Layout.fillWidth: true
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignLeft
                }
            }
        }
    }

    ListView {
        id: listView
        x: 20
        y: 43
        width: 247
        height: 257
        model: listModel
        delegate: contactDelegate

    }

    Button {
        id: nextButton
        x: 20
        y: 413
        width: 247
        height: 46
        text: qsTr("Next")

        enabled: firmwareB1OK === 1 && firmwareB2OK === 1

        contentItem: Text {
            text: nextButton.text
            font.pixelSize: 24
            opacity: enabled ? 1.0 : 0.3
            color: nextButton.down ? "#ffffff" : "#000000"
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            elide: Text.ElideRight
            anchors.fill: parent
        }
        background: Rectangle {
                  implicitWidth: 100
                  implicitHeight: 40
                  opacity: enabled ? 1 : 0.3

                  color: nextButton.down ? "#565656" : "#adadad"
                  border.width: 0
                  radius: 2
              }

        onClicked: nextPage()
    }

    DropShadow {
        id: dropShadow
        x: 273
        y: 8
        width: 439
        height: 433
        color: "#222222"
            horizontalOffset: 0
            verticalOffset: 0
            radius: 9
            spread: 0.1
            fast: true
            cached: false
            transparentBorder: true
            samples: 19
            source: imagerectangle
    }

    Rectangle {
        id: imagerectangle
        x: 273
        y: 8
        width: 439
        height: 433
        color: "#ffffff"

        Image {
            id: image
            anchors.fill: parent
            fillMode: Image.PreserveAspectFit

            source: "images/instruct.png"
        }
    }

    GridLayout{
        x: 20
        y: 362
        width: 247
        height: 16
        columns: 2
        Text {
            font.pixelSize: 16
            text: qsTr(firmwareB1)
            wrapMode: Text.WrapAnywhere
        }

        CustomProgressBar {
            id: progressBarB1

            Layout.fillWidth: true
            Layout.fillHeight: true
        }

        Text {
            font.pixelSize: 16
            text: qsTr(firmwareB2)
            wrapMode: Text.WrapAnywhere
        }
        CustomProgressBar {
            id: progressBarB2

            Layout.fillWidth: true
            Layout.fillHeight: true

        }
    }

    Text {
        id: text2
        x: 70
        y: 327

        text: qsTr("Download File")
        font.pixelSize: 24
    }

    Rectangle {
        id: rectangle
        x: 143
        y: 184
        width: 2
        height: 247
        rotation: 90
        gradient: Gradient {
                GradientStop {
                    position: 0
                    color: "#ffffff"
                }

                GradientStop {
                    position: 0.500
                    color: "#727272"
                }

                GradientStop {
                    position: 1
                    color: "#ffffff"
                }
            }
    }

}
