import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.0
import Tepari.drenchgunupdater 1.0

Item {
    id: page3
    width: 720
    signal updateFirmwareNames(string firmware1,string firmware2)
    signal backSignal()

    function setUseLocalFile(firmware1,firmware2){
        drenchGunUpdater.setUseLocalFile(firmware1,firmware2)
    }

    ChooseFirmware{
        id: chooseFirmware
        z: 1
        anchors.fill: parent
        visible: false
        onUseFirmware: {
            drenchGunUpdater.setFirmwareFileName(firmware)
            drenchGunUpdater.resumeThread()
        }
    }

    Image {
        id: image
        x: 293
        y: 12
        width: 200
        height: 200
        fillMode: Image.PreserveAspectCrop
        z: -1
        anchors.fill: parent
        source: "images/G10-0.jpg"
    }

    DrenchGunUpdater{
        id: drenchGunUpdater
        onRequestChooseFirmwareName: chooseFirmware.visible = true

        onSendFileSize: sendProgressBar.to = fileSize
        onProgress: sendProgressBar.value = pos
        onSendStatus: statusText.text = status        
        onSendFirmwareNames: {
            page3.updateFirmwareNames(firmware1,firmware2)
            chooseFirmware.firmware1 = firmware1
            chooseFirmware.firmware2 = firmware2
        }
    }



    ColumnLayout {
        id: columnLayout
        x: 518
        y: 337
        width: 194
        height: 135
        Text {
            id: text6
            text: qsTr("SYSTEM INFORMATION")
            font.pixelSize: 15
        }
        RowLayout {
            id: rowLayout
            width: 160
            height: 17

            Text {
                id: text2
                width: 60
                height: 16
                text: qsTr("Ip Address:")
                horizontalAlignment: Text.AlignHCenter
                font.pixelSize: 12
            }

            Text {
                id: text7
                text: drenchGunUpdater.ip

                font.bold: true
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
                Layout.fillWidth: true
                font.pixelSize: 12
            }
        }
        RowLayout {
            id: rowLayout1
            width: 160
            height: 23


            Text {
                id: text4
                text: qsTr("Device Firmware")
                verticalAlignment: Text.AlignVCenter
                font.pixelSize: 12
            }

            Text {
                id: firmwareDevice
                width: 270
                height: 32
                font.pixelSize: 12
                text: drenchGunUpdater.firmwareDevice
                wrapMode: Text.WrapAnywhere
                Layout.fillWidth: true
                font.bold: true
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
            }
        }
        RowLayout {
            id: rowLayout2
            width: 160
            height: 24

            Text {
                id: text5
                text: qsTr("Firmware Update")
                Layout.fillWidth: false
                font.pixelSize: 12
            }

            Text {
                id: sendFirmware
                width: 270
                height: 32
                font.pixelSize: 12
                text: drenchGunUpdater.firmwareFileName
                wrapMode: Text.WrapAnywhere
                Layout.fillWidth: true
                font.bold: true
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
            }
        }
    }

    Item{
        id:content
        enabled: !chooseFirmware.visible || !connectToServer.visible

        Button {
            id: updatebutton
            x: 8
            y: 153
            width: 270
            height: 55
            text: "Start Update"
            contentItem: Text {
                text: updatebutton.text
                font.pixelSize: 24
                opacity: enabled ? 1.0 : 0.3
                color: updatebutton.down ? "#ffffff" : "#000000"
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                elide: Text.ElideRight
                anchors.fill: parent
            }

            background: Rectangle {
                      implicitWidth: 100
                      implicitHeight: 40
                      opacity: enabled ? 1 : 0.3

                      color: updatebutton.down ? "#565656" : "#adadad"
                      border.width: 0
                      radius: 2
            }

            onClicked: drenchGunUpdater.startSend()
        }

        Text {
            id: text1
            x: 8
            y: 220
            width: 86
            height: 33
            text: qsTr("Status :")
            font.pixelSize: 24
        }

        Text {
            id: statusText
            x: 100
            y: 220
            width: 220
            height: 140
            wrapMode: Text.WordWrap
            font.pixelSize: 24
        }

        CustomProgressBar {
            id: sendProgressBar
            x: 8
            y: 366
            width: 312
            height: 36
            value: 0
        }

        Button {
            id: backbutton
            x: 60
            y: 408
            width: 166
            height: 43
            text: "Back"
            contentItem: Text {
                color: backbutton.down ? "#ffffff" : "#000000"
                text: backbutton.text
                opacity: enabled ? 1.0 : 0.3
                anchors.fill: parent
                horizontalAlignment: Text.AlignHCenter
                font.pixelSize: 24
                elide: Text.ElideRight
                verticalAlignment: Text.AlignVCenter
            }
            background: Rectangle {
                color: backbutton.down ? "#565656" : "#adadad"
                radius: 2
                border.width: 0
                opacity: enabled ? 1 : 0.3
                implicitWidth: 100
                implicitHeight: 40
            }
            onClicked: backSignal()
        }

    }
}
