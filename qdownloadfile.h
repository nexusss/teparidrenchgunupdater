#ifndef QDOWNLOADFILE_H
#define QDOWNLOADFILE_H

#include <QObject>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QUrl>
#include <QFile>

class QDownloadFile : public QObject
{
    Q_OBJECT
    QUrl url;
    QNetworkAccessManager *manager;
    QNetworkReply *reply;
    QFile *file;


    void startRequest(QUrl url);
public:
    explicit QDownloadFile(QObject *parent = 0);

signals:
    void successDownload();
    void failedDownload();
    void updateDownloadProgress(qint64 bytesReceived, qint64 bytesTotal);
public slots:
    void startDownload(QString url, QString filename);
    void httpReadyRead();
    void httpDownloadFinished();
    void cancelDownload();
};

#endif // QDOWNLOADFILE_H
