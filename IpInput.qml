import QtQuick 2.0
import QtQuick.Layouts 1.1

Rectangle {
    id: ipInput
    property string text: okt1.text + "." + okt2.text + "." + okt3.text + "." + okt4.text
    property int pixelSize: 12
    color: "#00000000"

    onTextChanged: {
        var oktList = text.split(".")

            okt1.text = oktList[0]
            okt2.text = oktList[1]
            okt3.text = oktList[2]
            okt4.text = oktList[3]
    }

    RowLayout {
        id: rowLayout
        anchors.fill: parent

        TextInput {
            id: okt1

            horizontalAlignment: Text.AlignHCenter
            Layout.fillHeight: true
            Layout.fillWidth: true
            font.pixelSize: pixelSize
            validator: IntValidator{bottom: 1; top: 255}
            onTextChanged: ipInput.text = okt1.text + "." + okt2.text + "." + okt3.text + "." + okt4.text
        }

        Text {

            text: qsTr(".")
            font.pixelSize: pixelSize
        }
        TextInput {
            id: okt2

            horizontalAlignment: Text.AlignHCenter
            Layout.fillHeight: true
            Layout.fillWidth: true
            font.pixelSize: pixelSize
            validator: IntValidator{bottom: 0; top: 255}
            onTextChanged: ipInput.text = okt1.text + "." + okt2.text + "." + okt3.text + "." + okt4.text
        }

        Text {

            text: qsTr(".")
            font.pixelSize: pixelSize
        }
        TextInput {
            id: okt3

            horizontalAlignment: Text.AlignHCenter
            Layout.fillHeight: true
            Layout.fillWidth: true
            font.pixelSize: pixelSize
            validator: IntValidator{bottom: 0; top: 255}
            onTextChanged: ipInput.text = okt1.text + "." + okt2.text + "." + okt3.text + "." + okt4.text
        }

        Text {

            text: qsTr(".")
            font.pixelSize: pixelSize
        }
        TextInput {
            id: okt4

            horizontalAlignment: Text.AlignHCenter
            Layout.fillHeight: true
            Layout.fillWidth: true
            font.pixelSize: pixelSize
            validator: IntValidator{bottom: 0; top: 255}
            onTextChanged: ipInput.text = okt1.text + "." + okt2.text + "." + okt3.text + "." + okt4.text
        }

    }

}
