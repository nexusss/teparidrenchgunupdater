#ifndef DRENCHGUNUPDATER_H
#define DRENCHGUNUPDATER_H

#include <QObject>
#include <QThread>
#include "ymodemsendviatcpip.h"


class DrenchGunUpdater : public QObject
{
    Q_OBJECT
    YmodemSendViaTCPIP *ymodem;
    QThread ymodemthread;

    Q_PROPERTY(QString ip READ ip WRITE setIp NOTIFY ipChange)
    Q_PROPERTY(QString firmwareFileName READ firmwareFileName WRITE setFirmwareFileName NOTIFY firmwareFileNameChange)
    Q_PROPERTY(QString firmwareDevice READ firmwareDevice NOTIFY firmwareDeviceChange)

public:
    explicit DrenchGunUpdater(QObject *parent = 0);
    QString ip();
    QString firmwareFileName();
    QString firmwareDevice();
    quint16 port();    
    ~DrenchGunUpdater();

signals:
    void sendComplete();
    void sendStatus(QString status);
    void progress(double pos);
    void sendFileSize(double fileSize);
    void requestChooseFirmwareName();
    void resumeThread();
    void firmwareFileNameChange(QString firmwareFileName);
    void firmwareDeviceChange(QString firmwareDevice);    
    void ipChange(QString ip);
    void sendFirmwareNames(QString firmware1,QString firmware2);

public slots:   
    void setIp(QString ip);
    void setPort(quint16 port);
    void setFirmwareFileName(QString firmwareFileName);
    void startSend();
    void setUseLocalFile(bool useLocalFileB1, bool useLocalFileB2);
};

#endif // DRENCHGUNUPDATER_H
