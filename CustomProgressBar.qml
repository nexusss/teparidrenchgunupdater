import QtQuick 2.0
import QtQuick.Controls 2.1

ProgressBar {
      id: control
      value: 0

      background: Rectangle {
          implicitWidth: 200
          implicitHeight: 6
          color: "#feb013"
          radius: 3
      }

      contentItem: Item {
          implicitWidth: 200
          implicitHeight: 6

          Text {
              text: Math.round((control.value / control.to) * 100) + "%"
              verticalAlignment: Text.AlignVCenter
              horizontalAlignment: Text.AlignHCenter
              anchors.fill: parent
              font.pixelSize: 12
              z:2
          }

          Rectangle {
              width: control.visualPosition * parent.width
              height: parent.height
              radius: 2
              color: "#17a81a"
              z:1
          }
      }
  }
