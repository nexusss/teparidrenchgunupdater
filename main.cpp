#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include "drenchgunupdater.h"
#include <qmldownloadfile.h>

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QGuiApplication app(argc, argv);

    qmlRegisterType<DrenchGunUpdater>("Tepari.drenchgunupdater", 1, 0, "DrenchGunUpdater");
    qmlRegisterType<QmlDownloadFile>("Tepari.downloader", 1, 0, "TepariDownloader");

    QCoreApplication::setAttribute(Qt::AA_UseSoftwareOpenGL);

    QQmlApplicationEngine engine;
    engine.load(QUrl(QLatin1String("qrc:/main.qml")));

    return app.exec();
}
