import QtQuick 2.0
import QtQuick.Controls 2.1

Item {
    signal nextPage()

    Image {
        id: image
        x: 293
        y: 12
        width: 200
        height: 200
        fillMode: Image.PreserveAspectCrop
        z: -1
        anchors.fill: parent
        source: "images/G10-0.jpg"
    }

    Text {
        id: text1
        x: 20
        y: 175
        text: qsTr("Getting Started:")
        font.pixelSize: 24
    }

    Text {
        id: text2
        x: 20
        y: 220
        width: 253
        height: 148
        text: qsTr("Please ensure your computer is connected to the internet before clicking next")
        wrapMode: Text.WordWrap
        font.pixelSize: 24
    }

    Button {
        id: nextButton
        x: 20
        y: 395
        width: 247
        height: 46
        text: qsTr("Next")

        contentItem: Text {
                  text: nextButton.text
                  font.pixelSize: 24
                  opacity: enabled ? 1.0 : 0.3
                  color: nextButton.down ? "#ffffff" : "#000000"
                  horizontalAlignment: Text.AlignHCenter
                  verticalAlignment: Text.AlignVCenter
                  elide: Text.ElideRight
                  anchors.fill: parent
              }
        background: Rectangle {
                  implicitWidth: 100
                  implicitHeight: 40
                  opacity: enabled ? 1 : 0.3

                  color: nextButton.down ? "#565656" : "#adadad"
                  border.width: 0
                  radius: 2
              }

        onClicked: nextPage()
    }

}
