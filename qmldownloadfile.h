#ifndef QMLDOWNLOADFILE_H
#define QMLDOWNLOADFILE_H

#include <QObject>
#include <QThread>
#include "qdownloadfile.h"

class QmlDownloadFile : public QObject
{
    Q_OBJECT
    QDownloadFile *qdownloadFile;
    QThread downloadthread;
    QString savePath;
    const QString urlAddress = "https://www.trisport.co.nz/EGFirmware/";

public:
    explicit QmlDownloadFile(QObject *parent = 0);
    ~QmlDownloadFile();

signals:
    void successDownload();
    void failedDownload();
    void updateDownloadProgress(qint64 bytesReceived, qint64 bytesTotal);
public slots:
    void startDownload(QString filename);

};

#endif // QMLDOWNLOADFILE_H
