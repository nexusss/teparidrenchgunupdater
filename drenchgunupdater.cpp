#include "drenchgunupdater.h"

DrenchGunUpdater::DrenchGunUpdater(QObject *parent) : QObject(parent)
{

    ymodem = new YmodemSendViaTCPIP();
    ymodem->moveToThread(&ymodemthread);

    connect(&ymodemthread,&QThread::started,ymodem,&YmodemSendViaTCPIP::init);

    connect(ymodem,&YmodemSendViaTCPIP::sendComplete,this,&DrenchGunUpdater::sendComplete);
    connect(ymodem,&YmodemSendViaTCPIP::sendStatus,this,&DrenchGunUpdater::sendStatus);
    connect(ymodem,&YmodemSendViaTCPIP::progress,this,&DrenchGunUpdater::progress);
    connect(ymodem,&YmodemSendViaTCPIP::sendFileSize,this,&DrenchGunUpdater::sendFileSize);
    connect(ymodem,&YmodemSendViaTCPIP::requestChooseFirmwareName,this,&DrenchGunUpdater::requestChooseFirmwareName);
    connect(this,&DrenchGunUpdater::resumeThread,ymodem,&YmodemSendViaTCPIP::resumeThread);
    connect(ymodem,&YmodemSendViaTCPIP::firmwareFileNameChange,this,&DrenchGunUpdater::firmwareFileNameChange);
    connect(ymodem,&YmodemSendViaTCPIP::firmwareDeviceChange,this,&DrenchGunUpdater::firmwareDeviceChange);

    connect(ymodem,&YmodemSendViaTCPIP::sendFirmwareNames,this,&DrenchGunUpdater::sendFirmwareNames);


    ymodemthread.start();

}

QString DrenchGunUpdater::ip(){
    return ymodem->ip();
}

QString DrenchGunUpdater::firmwareFileName(){
    return ymodem->firmwareFileName();
}

//QString DrenchGunUpdater::firmware1(){
//    return ymodem->firmware1();
//}

//QString DrenchGunUpdater::firmware2(){
//    return ymodem->firmware2();
//}

QString DrenchGunUpdater::firmwareDevice(){
    return ymodem->firmwareDevice();
}

quint16 DrenchGunUpdater::port(){
    return ymodem->port();
}

void DrenchGunUpdater::setIp(QString ip){
    QMetaObject::invokeMethod(ymodem,"setIp",Q_ARG(QString, ip));
}

void DrenchGunUpdater::setPort(quint16 port){
    QMetaObject::invokeMethod(ymodem,"setPort",Q_ARG(quint16, port));
}

void DrenchGunUpdater::setFirmwareFileName(QString firmwareFileName){
    QMetaObject::invokeMethod(ymodem,"setFirmwareFileName",Q_ARG(QString, firmwareFileName));
}

void DrenchGunUpdater::startSend(){
    QMetaObject::invokeMethod(ymodem,"startSend");
}

void DrenchGunUpdater::setUseLocalFile(bool useLocalFileB1, bool useLocalFileB2){
    QMetaObject::invokeMethod(ymodem,"setUseLocalFile",Q_ARG(bool, useLocalFileB1),Q_ARG(bool, useLocalFileB2));
}

DrenchGunUpdater::~DrenchGunUpdater(){
    ymodem->cancelSend();
    ymodemthread.quit();
    ymodemthread.wait();
}
