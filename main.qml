import QtQuick 2.7
import QtQuick.Window 2.0
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.0
import QtQuick.Dialogs 1.2
import Tepari.drenchgunupdater 1.0

ApplicationWindow {
    id: applicationWindow1
    visible: true
    width: 720
    height: 480
    title: qsTr("Te Pari Drench Gun Updater v 1.1")

    SwipeView{
        id: swipeView
        anchors.fill: parent
        interactive: false
        Page1{
            onNextPage: {
                swipeView.currentIndex = 1
                page2.startDownloadFirmware()
            }
        }

        Page2{
            id: page2
            onNextPage: swipeView.currentIndex = 2
            onSetLocalFile: page3.setUseLocalFile(localFirmwareB1,localFirmwareB2)
        }

        Page3{
            id: page3
            onUpdateFirmwareNames: {
                page2.firmwareB1 = firmware1
                page2.firmwareB2 = firmware2
            }
            onBackSignal: swipeView.currentIndex = 1
        }
    }
}
