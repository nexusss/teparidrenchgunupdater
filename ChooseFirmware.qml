import QtQuick 2.0
import QtQuick.Controls 2.1

import QtQuick.Layouts 1.1

Rectangle {
    id: root
    property string firmware1: ""
    property string firmware2: ""

    signal useFirmware(string firmware)
    color: "#00000000"

    Rectangle{
        color: "#a4a4a4"
        opacity: 0.8
        anchors.fill: parent
    }


    Rectangle{
        width: 250
        height: 150
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter
        GroupBox {

            anchors.fill: parent

            ColumnLayout {
                id: columnLayout
                anchors.fill: parent

                RadioButton {
                    id: firmwareRadio1
                    text: qsTr(firmware1)

                }

                RadioButton {
                    id: firmwareRadio2
                    text: qsTr(firmware2)
                }

                Button {
                    id: button
                    text: qsTr("Apply")
                    Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                    onClicked: {
                        if(firmwareRadio1.checked)
                            useFirmware(firmware1)
                        else if(firmwareRadio2.checked)
                            useFirmware(firmware2)
                        else
                            useFirmware("")

                        root.visible = false
                    }
                }
            }
        }
    }
}
