#include "qdownloadfile.h"

QDownloadFile::QDownloadFile(QObject *parent) : QObject(parent)
{
    file = nullptr;
}


void QDownloadFile::startDownload(QString url,QString filename){
    manager = new QNetworkAccessManager(this);

        // get url
    this->url = QUrl(url);


    if(file == nullptr)
        delete file;

    file = new QFile(filename);

    if (!file->open(QIODevice::WriteOnly)) {
        delete file;
        file = nullptr;
        return;
    }

        // used for progressDialog
        // This will be set true when canceled from progress dialog


    // download button disabled after requesting download
    startRequest(url);
}

void QDownloadFile::startRequest(QUrl url)
{
    qDebug() << "start request";
    // get() method posts a request
    // to obtain the contents of the target request
    // and returns a new QNetworkReply object
    // opened for reading which emits
    // the readyRead() signal whenever new data arrives.
    reply = manager->get(QNetworkRequest(url));

    // Whenever more data is received from the network,
    // this readyRead() signal is emitted
    connect(reply, &QNetworkReply::readyRead,this,&QDownloadFile::httpReadyRead);

    // Also, downloadProgress() signal is emitted when data is received
    connect(reply,&QNetworkReply::downloadProgress,this,&QDownloadFile::updateDownloadProgress);


    // This signal is emitted when the reply has finished processing.
    // After this signal is emitted,
    // there will be no more updates to the reply's data or metadata.
    connect(reply,&QNetworkReply::finished,this,&QDownloadFile::httpDownloadFinished);
}

void QDownloadFile::httpDownloadFinished()
{

    file->flush();
    file->close();

    // get redirection url
    QVariant redirectionTarget = reply->attribute(QNetworkRequest::RedirectionTargetAttribute);
    if (reply->error()) {
        file->remove();
        emit failedDownload();
    } else if (!redirectionTarget.isNull()) {
        QUrl newUrl = url.resolved(redirectionTarget.toUrl());

            url = newUrl;
            reply->deleteLater();
            file->open(QIODevice::WriteOnly);
            file->resize(0);
            startRequest(url);
            return;
    }else{
        emit successDownload();
    }

    reply->deleteLater();
    reply = nullptr;
    delete file;
    file = nullptr;

    manager = 0;
}

void QDownloadFile::httpReadyRead()
{
    // this slot gets called every time the QNetworkReply has new data.
    // We read all of its new data and write it into the file.
    // That way we use less RAM than when reading it at the finished()
    // signal of the QNetworkReply
    if (file)
        file->write(reply->readAll());
}

void QDownloadFile::cancelDownload()
{
    qDebug() << "cancel";
    if(reply != nullptr)
        reply->abort();
}
