#ifndef YMODEMPROTOCOL_H
#define YMODEMPROTOCOL_H

#include <QObject>
#include <QFile>

#define SOH  0x01
#define STX  0x02
#define EOT  0x04
#define ACK  0x06
#define NAK  0x15
#define CAN  0x18
#define CTRLZ 0x1A
#define CTRLZ2 '\0'

//enum class MESSAGE{SOH = 0x01,STX = 0x02,EOT = 0x04,ACK = 0x06,NAK = 0x15,CAN = 0x18,CTRLZ = 0x1a};

enum class YMODEMResponce {
        MODEM_SUCCESS_UPDATE = 0,
        MODEM_NOT_SUCCESS_UPDATE = -7,
        MODEM_PACKET_FAILED = -1,
        MODEM_REMOTE_CANCEL = -2,
        MODEM_EOT_ERROR = -3,
        MODEM_NOT_READY = -4,
        MODEM_RESTART = -5,
        MODEM_SUCCESS_WRITE = -6,
};

class YmodemProtocol : public QObject
{
    Q_OBJECT

public:
    explicit YmodemProtocol(QObject *parent = 0);
    const static quint32 payloadSize = 1024;


    unsigned short crc16_ccitt( const unsigned char *buf, int len );
    void checkResponse(const YMODEMResponce &response);
    QByteArray createClosePacket();
    QByteArray createFirstPayload(const QString &fileName, const QString &fileInfoSize, const QString &timeCreated);
    QByteArray createPayload(QIODevice *dev);
    QByteArray createPacket(const QByteArray &payload,const quint8 &sequenceNum);
signals:
    void sendResponseResult(QString response);
public slots:
};

#endif // YMODEMPROTOCOL_H
